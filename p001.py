def multiples_sum(n, limit):
    i = (limit - 1) // n
    return (i * n * (1 + i)) // 2


def solve(limit=1000):
    return multiples_sum(3, limit) + multiples_sum(5, limit) - multiples_sum(15, limit)


def solve_comprehension(limit=1000):
    return sum(i for i in range(limit) if i % 3 == 0 or i % 5 == 0)


assert solve(10) == 23
assert solve_comprehension(10) == 23
