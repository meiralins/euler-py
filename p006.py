def _sum_of_squares(n):
    return sum(n * n for n in range(1, n + 1))


def _square_of_sum(n):
    return sum(range(1, n + 1)) ** 2


def sum_of_squares(n):
    return (n * (2 * n + 1) * (n + 1)) // 6


def square_of_sum(n):
    return ((n * (n + 1)) // 2) ** 2


def solve(n=100):
    return square_of_sum(n) - sum_of_squares(n)


assert solve(10) == 2640
