from lib import factor_count


def triangular():
    n = 1
    s = 1
    while True:
        yield s
        n += 1
        s += n


def solve(n=500):
    for value in triangular():
        if factor_count(value) > n:
            return value
