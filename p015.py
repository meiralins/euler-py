from functools import reduce
from math import factorial
from operator import mul


def solve(n=20):
    """Anagrams of aabb - > (2n)! / (n! * n!)"""
    return reduce(mul, range(2 * n, n, -1)) // factorial(n)


assert solve(2) == 6
