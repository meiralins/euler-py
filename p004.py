from itertools import islice


def is_palindrome(n):
    s = str(n)
    return s == s[::-1]


def solve_brute(floor=100, ceil=1000):
    return max(
        a * b
        for a in range(floor, ceil)
        for b in range(a, ceil)
        if is_palindrome(a * b)
    )


assert solve_brute(10, 100) == 9009
