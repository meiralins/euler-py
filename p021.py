from functools import lru_cache

from lib import proper_divisors


@lru_cache(maxsize=2 ** 13)
def sum_of_divisors(n):
    return sum(proper_divisors(n))


def solve(limit=10000):
    return sum(
        x
        for x in range(limit)
        if sum_of_divisors(x) != x and x == sum_of_divisors(sum_of_divisors(x))
    )


assert sum_of_divisors(220) == 284
assert sum_of_divisors(284) == 220
