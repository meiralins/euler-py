from itertools import islice

from lib import prime_sieve


def solve(n=10001):
    primes = prime_sieve()
    for _ in islice(primes, n - 1):
        pass
    return next(primes)


assert solve(6) == 13
