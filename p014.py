from functools import lru_cache


@lru_cache(maxsize=2 ** 22)
def collatz_len(n):
    if n <= 1:
        return 1
    if n % 2:
        return 2 + collatz_len((3 * n + 1) // 2)
    else:
        return 1 + collatz_len(n // 2)


def solve(limit=1000000):
    return max((collatz_len(x), x) for x in range(limit // 2, limit))


assert collatz_len(13) == 10
