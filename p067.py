def get_data():
    with open("data/p067_triangle.txt") as f:
        return [[int(x) for x in line.strip().split()] for line in f]


def solve():
    d = get_data()
    N = len(d)
    for i in range(N - 2, -1, -1):
        for j, val in enumerate(d[i]):
            d[i][j] = val + max(d[i + 1][j], d[i + 1][j + 1])
    return d[0][0]
