from collections import defaultdict
from functools import partial, reduce
from itertools import takewhile
from math import log
from operator import ge, mul

from lib import prime_factors, prime_sieve


def max_power_below(n, limit):
    assert n < limit
    return n ** int(log(limit, n))


def solve(limit=20):
    factors = defaultdict(int)
    for i in range(1, limit + 1):
        for factor, count in prime_factors(i):
            factors[factor] = max(factors[factor], count)
    min_factors = (factor ** count for factor, count in factors.items())
    return reduce(mul, min_factors, 1)


def solve_primes(limit=20):
    primes = takewhile(partial(ge, limit), prime_sieve())
    return reduce(mul, (max_power_below(prime, limit) for prime in primes), 1)


assert 2520 == solve(10)
assert 2520 == solve_primes(10)
assert solve() == solve_primes()
