def solve():
    for c in range(997, 333, -1):
        for b in range(999 - c, 332, -1):
            a = 1000 - c - b
            if a ** 2 + b ** 2 == c ** 2:
                return (a, b, c, a * b * c)
