from functools import partial
from itertools import takewhile
from operator import ge


def fib_gen():
    a, b = 0, 1
    yield a
    while True:
        yield b
        a, b = b, b + a


def fib_pair_gen():
    a, b = 0, 2
    yield a
    while True:
        yield b
        a, b = b, 4 * b + a


def solve(limit=4000000):
    pairs = (n for n in fib_gen() if n % 2 == 0)
    limited_pairs = takewhile(partial(ge, limit), pairs)
    return sum(limited_pairs)


def solve_pairs(limit=4000000):
    limited_pairs = takewhile(partial(ge, limit), fib_pair_gen())
    return sum(limited_pairs)


assert solve(limit=90) == sum([2, 8, 34])
assert solve_pairs(limit=90) == sum([2, 8, 34])
assert solve() == solve_pairs()
