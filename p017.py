d = {
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine",
    10: "ten",
    11: "eleven",
    12: "twelve",
    13: "thirteen",
    14: "fourteen",
    15: "fifteen",
    16: "sixteen",
    17: "seventeen",
    18: "eighteen",
    19: "nineteen",
    20: "twenty",
    30: "thirty",
    40: "forty",
    50: "fifty",
    60: "sixty",
    70: "seventy",
    80: "eighty",
    90: "ninety",
    1000: "onethousand",
}


def letters(n):
    if n == 0:
        return 0
    if n == 1000:
        return len(d[1000])
    if n <= 20:
        return len(d[n])
    if n < 100:
        return len(d[10 * (n // 10)]) + letters(n % 10)
    if n % 100 == 0:
        return len(d[n // 100]) + len("hundred")

    return len(d[n // 100]) + len("hundredand") + letters(n % 100)


def solve(limit=1000):
    return sum(letters(n) for n in range(1, limit + 1))
