from functools import partial
from itertools import takewhile
from operator import gt

from lib import prime_sieve


def solve(limit=2000000):
    primes = takewhile(partial(gt, limit), prime_sieve())
    return sum(primes)
