from collections import defaultdict
from functools import reduce
from operator import mul


def prime_sieve():
    yield 2
    yield 3
    yield 5
    n = 5
    composite = defaultdict(list)
    composite[25] = [5]
    while True:
        n = n + 2 if n % 6 == 5 else n + 4
        if n not in composite:
            yield n
            composite[n * n] = [n]
        else:
            for prime in composite[n]:
                next_composite = n + 2 * prime
                if next_composite % 6 == 3:
                    next_composite = next_composite + 2 * prime
                composite[next_composite].append(prime)
            del composite[n]


def prime_factors(n):
    if n <= 0:
        return
    primes = prime_sieve()
    while n != 1:
        prime = next(primes)
        count = 0
        while n % prime == 0:
            n, count = n // prime, count + 1
        if count:
            yield prime, count


def factor_count(n):
    return reduce(mul, (1 + count for _, count in prime_factors(n)), 1)


def proper_divisors(n):
    if n <= 1:
        return [1]
    pf = prime_factors(n)
    divisors = [1]
    for prime, count in pf:
        divisors = [
            divisor * (prime ** i) for i in range(0, count + 1) for divisor in divisors
        ]
    divisors.pop()
    return divisors
